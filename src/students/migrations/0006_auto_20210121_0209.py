# Generated by Django 3.1.4 on 2021-01-21 02:09

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0005_auto_20210118_0025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='students',
            name='graduate_date',
            field=models.DateField(default=datetime.date(2021, 1, 21)),
        ),
    ]
