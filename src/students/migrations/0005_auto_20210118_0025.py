# Generated by Django 3.1.4 on 2021-01-18 00:25

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0004_auto_20210117_2335'),
    ]

    operations = [
        migrations.AlterField(
            model_name='students',
            name='graduate_date',
            field=models.DateField(default=datetime.date(2021, 1, 18)),
        ),
    ]
