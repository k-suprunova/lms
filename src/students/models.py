import datetime
import random

from core.models import Person

from django.db import models

from faker import Faker

# Create your models here.
from groups.models import Groups


class Students(Person):

    enroll_date = models.DateField(default=datetime.date.today)
    graduate_date = models.DateField(default=datetime.date.today())

    group = models.ForeignKey(
        to=Groups,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age}'

    @classmethod
    def generate_students(cls, count):
        faker = Faker()

        list_groups = list(Groups.objects.all())
        for _ in range(count):
            object1 = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(15, 105),
                group=random.choice(list_groups)
            )

            object1.save()
