from copy import copy

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentFilter, StudentUpdateForm
from students.models import Students

# Create your views here.


def get_students(request):
    students = Students.objects.all().select_related('group', 'headed_group').order_by('-id')

    filter = StudentFilter(  # noqa
        data=request.GET,
        queryset=students
    )

    return render(
        request=request,
        template_name='students-list.html',
        context={'filter': filter}
    )


class StudentListView(LoginRequiredMixin, ListView):
    students = Students.objects.all().select_related('group', 'headed_group').order_by('-id')
    model = Students
    template_name = 'students-list.html'
    login_url = reverse_lazy('accounts:login')
    paginate_by = 10
    context_object_name = 'students'

    def get_filter(self):
        return StudentFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['students'] = self.get_queryset()

        context['query_params'] = self.request.GET.urlencode()
        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()

        return context


class StudentCreateView(LoginRequiredMixin, CreateView):

    model = Students
    form_class = StudentUpdateForm
    success_url = reverse_lazy('list_students')
    template_name = 'students-create.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')


class StudentUpdateView(LoginRequiredMixin, UpdateView):

    model = Students
    form_class = StudentUpdateForm
    success_url = reverse_lazy('list_students')
    template_name = 'students-update.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')


class StudentDeleteView(LoginRequiredMixin, DeleteView):

    model = Students
    success_url = reverse_lazy('list_students')
    template_name = 'students-delete.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')
