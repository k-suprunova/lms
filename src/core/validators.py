import re

from django.core.exceptions import ValidationError


def validate_email_prohibited_domain(value):
    BLACKLIST = ['abc.efg', '000.com', 'black.list']  # noqa

    valid = True

    for bl_email in BLACKLIST:
        pattern = fr'^\S+@{bl_email}'
        if re.match(pattern, value):
            raise ValidationError(f'"{bl_email}" domain is prohibited.')

    return valid


def validate_phone_number(value):
    SHORT_LENGTH = 13  # noqa

    valid = True

    pattern = r'(\(\d\d\d\)|\+\d\d\(\d\d\d\))\d\d\d\-\d\d\d\d'

    if not re.match(pattern, value):
        raise ValidationError('Phone number is not correct')

    return valid
