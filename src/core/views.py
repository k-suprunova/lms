from django.http import HttpResponseRedirect  # noqa
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse  # noqa
# Create your views here.


def index(request):
    return render(
        request=request,
        template_name='index.html'
    )


# class ListView:
#
#     model = None
#     template_name = 'instance-list.html'
#
#     @classmethod
#     def get_context(cls, **kwargs):
#         return kwargs
#
#     @classmethod
#     def list_instance(cls, request, id):  # noqa
#         instance = get_object_or_404(cls.model, id=id)
#
#         filter = InstanceFilter(  # noqa
#             data=request.GET,
#             queryset=instance
#         )
#
#         return render(
#             request=request,
#             template_name=cls.template_name,
#             context={'filter': filter}
#         )

#
# class UpdateView:
#
#     model = None
#     form = None
#     success_url = None
#     template_name = 'instance-update.html'
#
#     @classmethod
#     def get_context(cls, **kwargs):
#         return kwargs
#
#     @classmethod
#     def update_instance(cls, request, id):  # noqa
#         instance = get_object_or_404(cls.model, id=id)
#
#         if request.method == 'POST':
#             form = cls.form(
#                 data=request.POST,
#                 instance=instance
#             )
#
#             if form.is_valid():
#                 form.save()
#                 return HttpResponseRedirect(reverse(cls.success_url))
#         else:
#             form = cls.form(
#                 instance=instance
#             )
#
#         return render(
#             request=request,
#             template_name=cls.template_name,
#             context=cls.get_context(**{
#                 'form': form,
#                 'instance': instance
#             })
#         )
#
#
# class CreateView:
#
#     model = None
#     form = None
#     success_url = None
#     template_name = 'instance-create.html'
#
#     @classmethod
#     def get_context(cls, **kwargs):
#         return kwargs
#
#     @classmethod
#     def create_instance(cls, request, id):  # noqa
#         instance = get_object_or_404(cls.model, id=id)
#
#         if request.method == 'POST':
#
#             form = cls.form(
#                 data=request.POST,
#                 instance=instance
#             )
#
#             if form.is_valid():
#                 form.save()
#                 return HttpResponseRedirect(reverse(cls.success_url))
#         else:
#             form = cls.form(
#                 instance=instance
#             )
#
#         return render(
#             request=request,
#             template_name=cls.template_name,
#             context=cls.get_context(**{
#                 'form': form,
#                 'instance': instance
#             })
#         )
#
#
# class DeleteView:
#
#     model = None
#     success_url = None
#     template_name = 'instance-update.html'
#
#     @classmethod
#     def get_context(cls, **kwargs):
#         return kwargs
#
#     @classmethod
#     def delete_instance(cls, request, id):  # noqa
#
#         instance = get_object_or_404(cls.model, id=id)
#
#         if request.method == 'POST':
#             instance.delete()
#             return HttpResponseRedirect(reverse(cls.success_url))
#
#         return render(
#             request=request,
#             template_name=cls.template_name,
#             context={
#                 'instance': instance,
#                 'title': f'{instance} delete'
#             }
#         )
