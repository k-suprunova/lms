import time

from core.models import Logger


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        start = time.time()

        response = self.get_response(request)

        elapsed = time.time() - start

        try:
            Logger(
                user=request.user,
                path=request.get_full_path(),
                execution_time=elapsed,
            ).save()
        except (ValueError, AttributeError):
            pass
        return response
