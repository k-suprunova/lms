from django.apps import AppConfig  # noqa


class CoreConfig(AppConfig):
    name = 'core'
