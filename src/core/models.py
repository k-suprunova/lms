from core.validators import validate_email_prohibited_domain, validate_phone_number

from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Person(models.Model):
    class Meta:
        abstract = True
    id = models.AutoField(primary_key=True)  # noqa
    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=84, null=False)
    age = models.IntegerField(null=True, default=21)
    email = models.EmailField(max_length=64, validators=[
        validate_email_prohibited_domain
    ])
    phone_number = models.CharField(max_length=20, null=True, validators=[
        validate_phone_number
    ])

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age}'

    def full_name(self):
        return f'{self.first_name} {self.last_name}'


class Logger(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    path = models.CharField(max_length=128)
    create_date = models.DateTimeField(auto_now_add=True)
    execution_time = models.FloatField()
    query_params = models.CharField(max_length=64, null=True)
    info = models.CharField(max_length=128, null=True)
