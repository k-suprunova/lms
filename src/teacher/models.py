import random

from core.models import Person

from django.db import models

from faker import Faker

# Create your models here.
from groups.models import Groups


class Teacher(Person):

    group = models.ForeignKey(
        to=Groups,
        null=True,
        on_delete=models.SET_NULL,
        related_name='teachers'
    )

    course_name = models.ManyToManyField(
        to='courses.Courses',
        null=True,
        related_name='teachers_course'
    )

    def __str__(self):
        return f'{self.first_name}, {self.last_name}, {self.age}, {self.email}'

    @classmethod
    def generate_teachers(cls, count):
        faker = Faker()

        for _ in range(count):
            object1 = cls(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                age=random.randint(15, 105),
                email=faker.email(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_of_birth()
            )

            object1.save()
