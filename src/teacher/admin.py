from django.contrib import admin # noqa

# Register your models here.
from teacher.models import Teacher


class TeacherAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['first_name', 'last_name', 'email', 'age']
    search_fields = ['first_name', 'last_name']
    list_filter = ['first_name', 'age']


admin.site.register(Teacher, TeacherAdmin)
