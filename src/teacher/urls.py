"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from teacher.views import TeacherCreateView, TeacherDeleteView, TeacherListView, TeacherUpdateView, \
    create_teachers, delete_teacher, update_teacher

urlpatterns = [
    # path('', get_teachers, name='list_teachers'),
    path('', TeacherListView.as_view(), name='list_teachers'),
    path('create', create_teachers, name='create_teachers'),
    path('create', TeacherCreateView.as_view(), name='create_teachers'),
    path('update/<int:id>', update_teacher, name='update_teachers'),
    path('update/<int:id>', TeacherUpdateView.as_view(), name='update_teachers'),
    path('delete/<int:id>', delete_teacher, name='delete_teachers'),
    path('delete/<int:id>', TeacherDeleteView.as_view(), name='delete_teachers')
]
