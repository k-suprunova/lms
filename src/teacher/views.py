from copy import copy

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from teacher.forms import TeacherCreateForm, TeacherFilter, TeacherUpdateForm
from teacher.models import Teacher
# Create your views here.


# def get_teachers(request):
#
#     teachers = Teacher.objects.all().order_by('-id')
#
#     params = [
#         'first_name',
#         'first_name__startswith',
#         'last_name__startswith',
#         'last_name',
#         'age',
#         'age__gt',
#         'age'
#     ]
#
#     for param_name in params:
#         param_value = request.GET.get(param_name)
#         if param_value:
#             teachers = teachers.filter(**{param_name: param_value})
#
#     return render(
#         request=request,
#         template_name='teacher-list.html',
#         context={'teachers': teachers}
#     )


class TeacherListView(ListView):
    model = Teacher
    template_name = 'teacher-list.html'
    paginate_by = 10
    context_object_name = 'teachers'

    def get_filter(self):
        return TeacherFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['teacher'] = self.get_queryset()

        context['query_params'] = self.request.GET.urlencode()
        if 'page' in self.request.GET:
            get_params = copy(self.request.GET)
            del get_params['page']
            context['query_params'] = get_params.urlencode()

        return context


def create_teachers(request):
    if request.method == 'POST':
        form = TeacherCreateForm(request.POST)
        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_teachers'))

    else:
        form = TeacherCreateForm()

    return render(
        request=request,
        template_name='teacher-create.html',
        context={'form': form}
    )


class TeacherCreateView(CreateView):
    model = Teacher
    form_class = TeacherUpdateForm
    success_url = 'list_teachers'
    template_name = 'teacher-create.html'
    pk_url_kwarg = 'id'


def update_teacher(request, id):  # noqa

    teacher = get_object_or_404(Teacher, id=id)

    if request.method == 'POST':

        form = TeacherUpdateForm(
            data=request.POST,
            instance=teacher
        )

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_teachers'))

    else:

        form = TeacherUpdateForm(instance=teacher)

    return render(
        request=request,
        template_name='teacher-update.html',
        context={'form': form}
    )


class TeacherUpdateView(UpdateView):

    model = Teacher
    form_class = TeacherUpdateForm
    success_url = 'list_teachers'
    template_name = 'teacher-update.html'
    pk_url_kwarg = 'id'


def delete_teacher(request, id):  # noqa
    teacher = get_object_or_404(Teacher, id=id)

    if request.method == 'POST':
        teacher.delete()
        return HttpResponseRedirect(reverse('list_teachers'))

    return render(
        request=request,
        template_name='teacher-delete.html',
        context={
            'teacher': teacher,
            'title': 'Teacher delete'
        }
    )


class TeacherDeleteView(DeleteView):

    model = Teacher
    success_url = 'list_teachers'
    template_name = 'teacher-delete.html'
    pk_url_kwarg = 'id'
