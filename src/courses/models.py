from django.db import models

# Create your models here.


class Courses(models.Model):
    course_name = models.CharField(max_length=64, null=False)

    class DIFFICULTY(models.TextChoices):
        TRIVIAL = '1', 'Trivial'
        EASY = '2', 'Easy'
        MEDIUM = '3', 'Medium'
        EXPERT = '4', 'Expert'
        HARDCORE = '5', 'Hardcore'

    difficulty = models.CharField(
        max_length=1,
        choices=DIFFICULTY.choices,
        default=DIFFICULTY.TRIVIAL
    )
