from classrooms.models import Classrooms

from django.forms import ModelForm


class ClassBaseForm(ModelForm):

    class Meta:
        model = Classrooms
        fields = '__all__'

    def as_div(self):
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class ClassCreateForm(ClassBaseForm):
    pass


class ClassUpdateForm(ClassBaseForm):
    pass
