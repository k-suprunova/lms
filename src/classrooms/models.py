from django.db import models

# Create your models here.
from groups.models import Groups


class Classrooms(models.Model):

    classroom_num = models.CharField(max_length=64, null=False)

    classroom = models.ManyToManyField(
        to=Groups,
        null=True,
        related_name='classroom'
    )
