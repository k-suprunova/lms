from classrooms.forms import ClassCreateForm, ClassUpdateForm
from classrooms.models import Classrooms

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse

# Create your views here.


def get_classroom(request):
    classroom = Classrooms.objects.all().order_by('-id')

    params = [
        'classroom_num'
    ]

    for param_name in params:
        param_value = request.GET.get(param_name)
        if param_value:
            classroom = classroom.filter(**{param_name: param_value})

    return render(
        request=request,
        template_name='classroom-list.html',
        context={'classroom': classroom}
    )


def create_classroom(request):
    if request.method == 'POST':

        form = ClassCreateForm(request.POST)

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_classroom'))

    else:

        form = ClassCreateForm()

    return render(
        request=request,
        template_name='classroom-create.html',
        context={'form': form}
    )


def update_classroom(request, id):  # noqa

    classroom = get_object_or_404(Classrooms, id=id)

    if request.method == 'POST':

        form = ClassUpdateForm(
            data=request.POST,
            instance=classroom
        )

        if form.is_valid():
            form.save()

            return HttpResponseRedirect(reverse('list_classroom'))

    else:

        form = ClassUpdateForm(instance=classroom)

    return render(
        request=request,
        template_name='classroom-update.html',
        context={
            'form': form,
            'classroom': classroom
        }
    )


def delete_classroom(request, id):  # noqa
    classroom = get_object_or_404(Classrooms, id=id)

    if request.method == 'POST':
        classroom.delete()
        return HttpResponseRedirect(reverse('list_classroom'))

    return render(
        request=request,
        template_name='classroom-delete.html',
        context={
            'classroom': classroom,
            'title': 'Delete classroom'
        }
    )
