from django.forms import ChoiceField, ModelForm

import django_filters

from groups.models import Groups

from students.models import Students


class GroupBaseForm(ModelForm):

    class Meta:
        model = Groups
        fields = '__all__'

    def as_div(self):
        return self._html_output(
            normal_row='<p%(html_class_attr)s>%(label)s %(field)s%(help_text)s</p>',
            error_row='%s',
            row_ender='</p>',
            help_text_html=' <span class="helptext">%s</span>',
            errors_on_separate_row=True,
        )


class GroupCreateForm(GroupBaseForm):
    pass


class GroupUpdateForm(GroupBaseForm):

    class Meta(GroupBaseForm.Meta):
        exclude = ['headman']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        students = [(st.id, st.full_name()) for st in self.instance.students.all()]
        self.fields['head_man'] = ChoiceField(choices=students, initial=self.instance.headman.id, required=False)

    def save(self, commit=True):
        headman = Students.objects.get(id=int(self.cleaned_data['head_man']))
        self.instance.headman = headman
        return super().save(commit)


class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Groups
        fields = {
            'group_name': ['exact'],
            'course': ['exact']
        }
