from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404  # noqa
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupFilter, GroupUpdateForm
from groups.models import Groups

# Create your views here.


class GroupListView(LoginRequiredMixin, ListView):
    model = Groups
    login_url = reverse_lazy('accounts:login')
    template_name = 'groups-list.html'
    paginate_by = 3
    context_object_name = 'groups'

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()
        context['groups'] = self.get_filter().qs.select_related('headman').prefetch_related('teachers')
        return context


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Groups
    form_class = GroupUpdateForm
    success_url = reverse_lazy('list_groups')
    template_name = 'groups-create.html',
    login_url = reverse_lazy('accounts:login')


class GroupUpdateView(LoginRequiredMixin, UpdateView):

    model = Groups
    form_class = GroupUpdateForm
    success_url = reverse_lazy('list_groups')
    template_name = 'groups-update.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')

    def get_filter(self):
        return GroupFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.select_related('group', 'headed_group').all()
        context['groups'] = self.get_filter().qs.select_related('headman').prefetch_related('teachers')
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):

    model = Groups
    success_url = reverse_lazy('list_groups')
    template_name = 'groups-delete.html'
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('accounts:login')
