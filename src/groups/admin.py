from django.contrib import admin # noqa

# Register your models here.
from groups.models import Groups

from students.models import Students

from teacher.models import Teacher


class StudentTable(admin.TabularInline):
    model = Students
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class TeacherTable(admin.TabularInline):
    model = Teacher
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    show_change_link = True
    max_num = 10


class GroupsAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['group_name', 'course', 'headman']
    search_fields = ['group_name', 'course', 'headman']
    list_filter = ['group_name', 'course']
    inlines = [StudentTable, TeacherTable]


admin.site.register(Groups, GroupsAdmin)
