"""lms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from groups.views import GroupCreateView, GroupDeleteView, GroupListView, GroupUpdateView

urlpatterns = [
    # path('', get_groups, name='list_groups'),
    path('', GroupListView.as_view(), name='list_groups'),
    # path('create', create_groups, name='create_groups'),
    path('create', GroupCreateView.as_view(), name='create_groups'),
    # path('update/<int:id>', update_group, name='update_groups'),
    path('update/<int:id>', GroupUpdateView.as_view(), name='update_groups'),
    # path('delete/<int:id>', delete_group, name='delete_groups'),
    path('delete/<int:id>', GroupDeleteView.as_view(), name='delete_groups')
]
