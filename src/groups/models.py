from django.db import models

# Create your models here.


class Groups(models.Model):

    group_name = models.CharField(max_length=64, null=False)
    info = models.CharField(max_length=64, null=True)

    headman = models.OneToOneField(
        to='students.Students',
        on_delete=models.SET_NULL,
        null=True,
        related_name='headed_group'
    )

    course = models.OneToOneField(
        to='courses.Courses',
        on_delete=models.SET_NULL,
        null=True,
        related_name='selected_course'
    )

    def __str__(self):
        return f'{self.group_name}, {self.info}'
